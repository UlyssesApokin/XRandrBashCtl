# XRandrBashCtl

XRandrBashCtl is a simple script to adjust the brightness of the external display.

# Download && Setup

````
$ git clone https://github.com/UlyssesApokin/XRandrBashCtl.git
````

````
$ cd XRandrBashCtl
````

````
$ chmod +x xrandrbashctl
````

*Bind hotkeys to given script commands in your window manager*

NAME_OF_MONITOR = one of 'xrandr --listmonitors'

Increase brightness
> ~/XRandrBashCtl/xrandrbashctl NAME_OF_MONITOR inc

Decrease brightness
> ~/XRandrBashCtl/xrandrbashctl NAME_OF_MONITOR dec
